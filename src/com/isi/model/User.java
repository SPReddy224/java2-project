package com.isi.model;

import com.isi.db.UserDBUtil;

public class User
{
	private String  id;
	private String fname;
	private String lname;
	private String email;
	private String password;
	
	public User()
	{
		
	}
	public User(String fname,String lname,String email,String password)
	{
		this.fname=fname;
		this.lname=lname;
		this.email=email;
		this.password=password;
	}
	public User(String email,String password)
	{
		this.email=email;
		this.password=password;
	}
	public void Register(UserDBUtil db)
	{
		try
		{
			db.Insert(this);
		}
		catch(Exception e)
		{
			e.getMessage();
		}
	}
	
	public boolean login(UserDBUtil db)
	{
		try
		{
			User founduser=db.finduser(this.email);
			System.out.println(founduser);
			if(founduser.getPassword().equals(this.getPassword()))
			{
				this.email=founduser.getEmail();
				this.password=founduser.getPassword();
				this.fname=founduser.getFname();
				this.lname=founduser.getLname();
				
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
}
