package com.isi.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import com.isi.model.User;



public class UserDBUtil
{
	private DataSource datasource;
	public UserDBUtil (DataSource datasource)
	{
		this.datasource=datasource;
	}
	public void Insert(User user) throws SQLException
	{
		System.out.println("insert");
		
		Connection conn=null;
		Statement stm=null;
		ResultSet res = null;
	
	try
	{
		conn=this.datasource.getConnection();
		String sql = "INSERT INTO user(email,password,fname,lname)"+ "VALUES('?','%?','%?','%?')";
		stm=conn.createStatement();
		stm.executeUpdate(sql);
	}
	finally
	{
		close(conn,stm,res);
	}
}

private void close(Connection conn,Statement smt,ResultSet res)
{
	try
	{
		if(res!=null)
		{
			res.close();
			
		}
		if(smt!=null)
		{
			smt.close();
		}
		if(conn!=null)
		{
			conn.close();
		}
	}
		catch(Exception exe)
		{
			exe.printStackTrace();
		}
}
public static boolean checkLogin(String fname, String lname, String email, String password) {
	// TODO Auto-generated method stub
	return false;
}
public User finduser(String email) throws SQLException 
{
	System.out.println("sai");
	Connection conn = null;
	
	ResultSet res=null;
	User founduser=null;
	try
	{
		conn=this.datasource.getConnection();
		String sql="SELECT * FROM user WHERE Email =?";
		PreparedStatement pstmt =  conn.prepareStatement(sql);
		pstmt.setString(1, email);
		res = pstmt.executeQuery();
		res.next();
		String fname=res.getString("fname").toString();
		String lname=res.getString("lname").toString();
		String Email=res.getString("email").toString();
		String password=res.getString("password").toString();
		founduser=new User(fname,lname,Email,password);
		return founduser;
		}
	finally
	{
		
	}
}
}
