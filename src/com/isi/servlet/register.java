package com.isi.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.isi.db.UserDBUtil;
import com.isi.model.User;

/**
 * Servlet implementation class register
 */
@WebServlet("/register")
public class register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public register() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    	@Resource(name="jdbc/mybook")
    	private DataSource datasource;
    	private UserDBUtil userdb;
    	
    	@Override
    	 public void init(ServletConfig config) throws ServletException
    	 {
    		super.init(config);
    		try
    		{
    			userdb=new UserDBUtil(datasource);
    		}
    		catch(Exception ex)
    		{
    			throw new ServletException(ex);
    		}
    	 }
	/**
	 * @param fname 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response, String fname) throws ServletException, IOException, SQLException {
		User newuser=new User();
		newuser.setFname(request.getParameter("fname"));
		newuser.setLname(request.getParameter("lname"));
		newuser.setEmail(request.getParameter("email"));
		newuser.setPassword(request.getParameter("password"));
		
		
		newuser.login(userdb);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
