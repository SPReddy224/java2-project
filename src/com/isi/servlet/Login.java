package com.isi.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.isi.db.*;

@WebServlet("/Login")
public class Login extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	String fname=request.getParameter("fname");
	String lname=request.getParameter("lname");
	String email=request.getParameter("email");
	String password=request.getParameter("userpass");
	System.out.println("sai");
	//request.getSession().invalidate();
	if(UserDBUtil.checkLogin(fname, lname, email, password))
	{
		response.sendRedirect("home.jsp");
		HttpSession session=request.getSession();
		session.setAttribute("fname",fname);
		session.setAttribute("lname",lname);
		session.setAttribute("email",email);
		session.setAttribute("password",password);	
	}
	
	
	else
	{
		response.sendRedirect("index.jsp");
	}
	
	}

	
}
